  @Library('common-shared') _

  pipeline {
    agent {
      kubernetes {
        label 'buildpack-agent'
        yaml '''
        apiVersion: v1
        kind: Pod
        spec:
          containers:
          - name: buildpack
            image: buildpack-deps:stable
            command:
            - cat
            env:
            - name: "MAVEN_OPTS"
              value: "-Duser.home=/home/jenkins"
            tty: true
            resources:
              limits:
                memory: "2Gi"
                cpu: "1"
              requests:
                memory: "2Gi"
                cpu: "1"
            volumeMounts:
            - name: tmp
              mountPath: /tmp
          - name: jnlp
            resources:
              limits:
                memory: "2Gi"
                cpu: "1"
              requests:
                memory: "2Gi"
                cpu: "1"
            volumeMounts:
            - name: tools
              mountPath: /opt/tools
            - name: m2-repo
              mountPath: /home/jenkins/.m2/repository
            - name: settings-xml
              mountPath: /home/jenkins/.m2/settings.xml
              subPath: settings.xml
              readOnly: true
            - name: settings-security-xml
              mountPath: /home/jenkins/.m2/settings-security.xml
              subPath: settings-security.xml
              readOnly: true
            - name: tmp
              mountPath: /tmp
          volumes:
          - name: tools
            persistentVolumeClaim:
              claimName: tools-claim-jiro-webdev
          - name: m2-repo
            emptyDir: {}
          - name: tmp
            emptyDir: {}
          - name: settings-xml
            secret:
              secretName: m2-secret-dir
              items:
              - key: settings.xml
                path: settings.xml
          - name: settings-security-xml
            secret:
              secretName: m2-secret-dir
              items:
              - key: settings-security.xml
                path: settings-security.xml
        '''
      }
    }

    environment {
      APP_NAME = 'geoip-rest-api'
      NAMESPACE = 'foundation-internal-webdev-apps'
      IMAGE_NAME = 'eclipsefdn/geoip-rest-api'
      CONTAINER_NAME = 'app'
      ENVIRONMENT = sh(
        script: """
          if [ "${env.BRANCH_NAME}" = "master" ]; then
            printf "production"
          else
            printf "${env.BRANCH_NAME}"
          fi
        """,
        returnStdout: true
      )
      TAG_NAME = sh(
        script: """
          GIT_COMMIT_SHORT=\$(git rev-parse --short ${env.GIT_COMMIT})
          if [ "${env.ENVIRONMENT}" = "" ]; then
            printf \${GIT_COMMIT_SHORT}-${env.BUILD_NUMBER}
          else
            printf ${env.ENVIRONMENT}-\${GIT_COMMIT_SHORT}-${env.BUILD_NUMBER}
          fi
        """,
        returnStdout: true
      )
    }

    options {
      buildDiscarder(logRotator(numToKeepStr: '10'))
    }

    tools {
        maven 'apache-maven-latest'
        jdk 'adoptopenjdk-hotspot-jdk11-latest'
    }

    triggers { 
      // build once a week to keep up with parents images updates
      cron('H H * * H') 
    }

    stages {
      stage('Build Java code') {
        steps {
          sh 'mvn -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn -B package'
        }
      }

      stage('Push package image to Nexus') {
        when {
          anyOf {
            environment name: 'ENVIRONMENT', value: 'production'
          }
        }
        steps {
          sh 'mvn -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn -B clean deploy'
        }
      }
    }

    post {
      always {
        sendNotifications currentBuild
      }
    }
  }
