package org.eclipsefoundation.core.config;

import java.io.StringReader;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.bind.Jsonb;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * Assure that required JsonB properties are properly configured for
 * serialization. The following cases are defined as guidelines in the Eclipse
 * API guide that should be covered (if possible) by this unit test:
 * 
 * <li>Property names must be ASCII snake_case (and never camelCase):
 * ^[a-z_][a-z_0-9]*$
 * <li>Array names should be pluralized
 * <li>Boolean property values must not be null
 * <li>Null values should have their fields removed
 * <li>Empty array values should not be null
 * <li>Date property values should conform to RFC 3339
 * 
 * @author Martin Lowe
 *
 */
@QuarkusTest
class JsonBConfigTest {

	Jsonb jsonb;

	@BeforeEach
	void createJsonB() {
		// would use injection, but ARC is not initialized fully for tests
		if (jsonb == null)
			jsonb = (new JsonBConfig()).getContext(Object.class);
	}

	@Test
	void serializeDateTime() {
		// expected output of date time object, formatted in UTC
		TestObj test = getTestSample();
		// convert to JSON for testing that doesn't rely on whitespace like str compares
		JsonObject json = Json.createReader(new StringReader(jsonb.toJson(test, TestObj.class))).readObject();
		Assertions.assertNotNull(json);
		Assertions.assertEquals("1996-12-20T00:39:57Z", json.getString("date_time"));
	}

	@Test
	void serializePropertiesLowerSnakeCase() {
		// expected output of date time object, formatted in UTC
		TestObj test = getTestSample();

		// convert to JSON for testing that doesn't rely on whitespace like str compares
		JsonObject json = Json.createReader(new StringReader(jsonb.toJson(test, TestObj.class))).readObject();
		Assertions.assertNotNull(json);
		Assertions.assertTrue(json.containsKey("date_time"));
		Assertions.assertFalse(json.containsKey("dateTime"));
	}

	
	void deserializeDateTime() {
		String sampleJson = getTestJSONString();
		TestObj actual = jsonb.fromJson(sampleJson, TestObj.class);
		TestObj expected = getTestSample();
		Assertions.assertNotNull(actual);
		// the 2 dates should equate and return 0 on comparison
		Assertions.assertEquals(0, expected.dateTime.compareTo(actual.dateTime));
	}

	@Test
	void deserializeInvalidPropertyName() {
		// use actual property name, rather than snake-cased version
		String sampleJson = "{\"dateTime\": \"1996-12-20T00:39:57Z\"}";
		TestObj actual = jsonb.fromJson(sampleJson, TestObj.class);
		// even with proper format, property isn't what is expected so it should be null
		Assertions.assertNull(actual.dateTime);
	}

	@Test
	void serializeNullValue() {
		// use null value, which should return a null value
		String sampleJson = "{\"date_time\": null}";
		TestObj actual = jsonb.fromJson(sampleJson, TestObj.class);
		Assertions.assertNull(actual.dateTime);
	}

	private TestObj getTestSample() {
		// create new object using the time specified in the test sample
		TimeZone tz = TimeZone.getTimeZone(ZoneId.of("GMT-08:00"));
		// create a calendar instance to represent the given date
		Calendar c = Calendar.getInstance(tz);
		c.set(1996, 11, 19, 16, 39, 57);

		return new TestObj(c.getTime());
	}

	private String getTestJSONString() {
		return "{\"date_time\": \"1996-12-20T00:39:57Z\"}";
	}

	/**
	 * Sample object type for serialization to ensure consistency and isolation of
	 * test cases
	 * 
	 * @author Martin Lowe
	 */
	public static class TestObj {
		Date dateTime;

		public TestObj() {
		}

		public TestObj(Date dateTime) {
			this.dateTime = dateTime;
		}

		/**
		 * @return the dateTime
		 */
		public Date getDateTime() {
			return dateTime;
		}

		/**
		 * @param dateTime the dateTime to set
		 */
		public void setDateTime(Date dateTime) {
			this.dateTime = dateTime;
		}

	}
}
