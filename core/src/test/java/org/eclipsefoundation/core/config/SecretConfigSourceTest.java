package org.eclipsefoundation.core.config;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * Tests the SecretConfigSource class using a temporary file created before
 * tests run.
 * 
 * @author Martin Lowe
 *
 */
@QuarkusTest
class SecretConfigSourceTest {
	private static final String KEY_1 = "sample.secret.property";
	private static final String VALUE_1 = "secret-value";
	private static final String KEY_2 = "eclipse.secret.token";
	private static final String VALUE_2 = "example";

	SecretConfigSource src;

	private static Path tmp;

	/**
	 * Create temporary test file for tests that are independent of external
	 * resources. This file should be deleted on exit of the test.
	 */
	@BeforeAll
	static void pre() {
		try {
			// should load 2 sample properties for testing
			SecretConfigSourceTest.tmp = Files.createTempFile("secret", ".properties");
			Files.write(SecretConfigSourceTest.tmp,
					(KEY_1 + "=" + VALUE_1 + "\n" + KEY_2 + "=" + VALUE_2).getBytes(StandardCharsets.UTF_8));
			// set the file to delete after test
			tmp.toFile().deleteOnExit();
		} catch (IOException e) {
			System.out.println("Error while setting up SecretConfigSource test property file" + e);
		}
	}

	@BeforeEach
	void beforeEach() {
		System.setProperty(SecretConfigSource.PROPERTY_NAME, tmp.toAbsolutePath().toString());
	}

	@Test
	void getPropertiesValid() {
		src = new SecretConfigSource();
		Map<String, String> secrets = src.getProperties();

		// test that the properties created in pre-test method can be read
		Assertions.assertEquals(VALUE_1, secrets.get(KEY_1));
		Assertions.assertEquals(VALUE_2, secrets.get(KEY_2));
	}

	@Test
	void getPropertiesNoPropertyValue() {
		System.setProperty(SecretConfigSource.PROPERTY_NAME, "");
		Map<String, String> secrets = new SecretConfigSource().getProperties();

		// should return an empty map if nothing to load
		Assertions.assertTrue(secrets.isEmpty());
	}

	@Test
	void getPropertiesNoFile() {
		// create a random unique file path that should be empty
		System.setProperty(SecretConfigSource.PROPERTY_NAME, "/tmp/" + UUID.randomUUID().toString() + ".tmp");
		Map<String, String> secrets = new SecretConfigSource().getProperties();

		// returned map of configuration values should be empty
		Assertions.assertTrue(secrets.isEmpty());
	}

	@Test
	void getPropertyValidKeys() {
		src = new SecretConfigSource();
		src.getProperties();

		Assertions.assertEquals(VALUE_1, src.getValue(KEY_1));
		Assertions.assertEquals(VALUE_2, src.getValue(KEY_2));
	}

	@Test
	void getPropertyNoPrepopulation() {
		// test raw getValue call without calling getProperties
		// checks that getValue will call for the source to populate
		Assertions.assertEquals(VALUE_1, new SecretConfigSource().getValue(KEY_1));
		Assertions.assertEquals(VALUE_2, new SecretConfigSource().getValue(KEY_2));
	}

	@Test
	void getPropertyNullKey() {
		src = new SecretConfigSource();
		src.getProperties();

		// should return null value for null key
		Assertions.assertEquals(null, src.getValue(null));
	}

	@Test
	void getPropertyMissingKey() {
		src = new SecretConfigSource();
		src.getProperties();

		// should return null value for missing key
		Assertions.assertEquals(null, src.getValue("not.present"));
	}
}
