package org.eclipsefoundation.core.test;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipsefoundation.core.helper.CSRFHelper;
import org.eclipsefoundation.core.model.AdditionalUserData;

/**
 * Test resource for testing basic responses and security/authentication measures.
 * 
 * @author Martin Lowe
 *
 */
@Path("/test")
@Produces(MediaType.APPLICATION_JSON)
public class TestResource {

    @Inject
    CSRFHelper csrf;
    @Inject
    AdditionalUserData aud;

    /**
     * Basic sample GET call that can be gated through CSRF if enabled to protect the data further.
     * 
     * @param passedCsrf the passed CSRF header value
     * @return empty ok response if CSRF is disabled or properly passed, 403 response otherwise.
     */
    @GET
    public Response get(@HeaderParam(value = CSRFHelper.CSRF_HEADER_NAME) String passedCsrf) {
        // check CSRF manually as its a get request
        csrf.compareCSRF(aud, passedCsrf);
        return Response.ok().build();
    }

    /**
     * Basic sample GET call that is not gated through CSRF. This could represent a user data endpoint, or a straight
     * CSRF endpoint to trigger the header to be returned if enabled.
     * 
     * @return empty ok response
     */
    @GET
    @Path("unguarded")
    public Response get() {
        // check CSRF manually as its a get request
        return Response.ok().build();
    }

    /**
     * Basic POST call that can be used to assist in validating filters.
     * 
     * @return empty ok response if CSRF is disabled or properly passed, 403 response otherwise.
     */
    @POST
    public Response post() {
        return Response.ok().build();
    }

    /**
     * Basic PUT call that can be used to assist in validating filters.
     * 
     * @return empty ok response if CSRF is disabled or properly passed, 403 response otherwise.
     */
    @PUT
    public Response put() {
        return Response.ok().build();
    }

    /**
     * Basic DELETE call that can be used to assist in validating filters.
     * 
     * @return empty ok response if CSRF is disabled or properly passed, 403 response otherwise.
     */
    @DELETE
    public Response delete() {
        return Response.ok().build();
    }
}
