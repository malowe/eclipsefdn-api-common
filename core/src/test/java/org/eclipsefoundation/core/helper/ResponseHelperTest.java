package org.eclipsefoundation.core.helper;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * Test suite for the ResponseHelper class.
 * 
 * @author Martin Lowe
 *
 */
@QuarkusTest
class ResponseHelperTest {

	@Inject
	ResponseHelper rh;

	@Test
	void constructorTest() {
		// available but not recommended (laggy)
		ResponseHelper inline = new ResponseHelper();
		// check that we can create the object
		Assertions.assertNotNull(inline);
		// check that injected object exists
		Assertions.assertNotNull(rh);
	}
}
