package org.eclipsefoundation.core.authenticated.helper;

import javax.inject.Inject;

import org.eclipsefoundation.core.exception.FinalUnauthorizedException;
import org.eclipsefoundation.core.helper.CSRFHelper;
import org.eclipsefoundation.core.model.AdditionalUserData;
import org.eclipsefoundation.core.test.AuthenticatedTestProfile;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;

/**
 * Test CSRF functionality from the helper directly using the authentication secured test profile.
 * 
 * @author Martin Lowe
 */
@QuarkusTest
@TestProfile(AuthenticatedTestProfile.class)
class CSRFHelperTest {

    @Inject
    CSRFHelper csrf;

    @Test
    void compareCSRF_validToken() {
        // generate a token to use in test
        String csrfToken = csrf.getNewCSRFToken();
        // create session object with given CSRF token
        AdditionalUserData aud = new AdditionalUserData();
        aud.setCsrf(csrfToken);

        // this should not throw as the tokens match
        Assertions.assertDoesNotThrow(() -> csrf.compareCSRF(aud, csrfToken));
    }

    @Test
    void compareCSRF_invalidToken() {
        // generate a token to use in test
        String csrfToken = csrf.getNewCSRFToken();
        // create session object with given CSRF token
        AdditionalUserData aud = new AdditionalUserData();
        aud.setCsrf(csrfToken);

        // this should throw as the tokens are not the same
        Assertions.assertThrows(FinalUnauthorizedException.class, () -> csrf.compareCSRF(aud, "some-other-value"));
    }

    @Test
    void compareCSRF_noSubmittedToken() {
        // generate a token to use in test
        String csrfToken = csrf.getNewCSRFToken();
        // create session object with given CSRF token
        AdditionalUserData aud = new AdditionalUserData();
        aud.setCsrf(csrfToken);

        // this should throw as the tokens are not the same
        Assertions.assertThrows(FinalUnauthorizedException.class, () -> csrf.compareCSRF(aud, null));

        // reset token value as its cleared between requests
        aud.setCsrf(csrfToken);
        Assertions.assertThrows(FinalUnauthorizedException.class, () -> csrf.compareCSRF(aud, ""));
    }

    @Test
    void compareCSRF_noGeneratedToken() {
        // simulates a session object with no CSRF data (no previous calls)
        AdditionalUserData aud = new AdditionalUserData();

        String sampleCSRF = csrf.getNewCSRFToken();
        Assertions.assertThrows(FinalUnauthorizedException.class, () -> csrf.compareCSRF(aud, null));
        Assertions.assertThrows(FinalUnauthorizedException.class, () -> csrf.compareCSRF(aud, ""));
        Assertions.assertThrows(FinalUnauthorizedException.class, () -> csrf.compareCSRF(aud, sampleCSRF));
    }
}
