package org.eclipsefoundation.core.request;

import java.io.IOException;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.exception.FinalUnauthorizedException;
import org.eclipsefoundation.core.helper.CSRFHelper;
import org.eclipsefoundation.core.model.AdditionalUserData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.undertow.httpcore.HttpMethodNames;

/**
 * Creates a security layer in front of mutation requests to require CSRF tokens (if enabled). This layer does not
 * perform the check of the token in-case there are other conditions that would rebuff the request.
 * 
 * @author Martin Lowe
 */
@Provider
public class CSRFSecurityFilter implements ContainerRequestFilter {
    public static final Logger LOGGER = LoggerFactory.getLogger(CSRFSecurityFilter.class);

    @ConfigProperty(name = "security.csrf.enabled", defaultValue = "false")
    boolean csrfEnabled;

    @Inject
    CSRFHelper csrf;
    @Inject
    AdditionalUserData aud;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        if (csrfEnabled) {
            // check if the HTTP method indicates a mutation
            String method = requestContext.getMethod();
            if (HttpMethodNames.DELETE.equals(method) || HttpMethodNames.POST.equals(method)
                    || HttpMethodNames.PUT.equals(method)) {
                // check csrf token presence (not value)
                String token = requestContext.getHeaderString(CSRFHelper.CSRF_HEADER_NAME);
                if (token == null || "".equals(token.trim())) {
                    throw new FinalUnauthorizedException("No CSRF token passed for mutation call, refusing connection");
                } else {
                    // run comparison. If error, exception will be thrown
                    csrf.compareCSRF(aud, token);
                }
            }
        }
    }
}
