package org.eclipsefoundation.core.service.impl;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.helper.ParameterHelper;
import org.eclipsefoundation.core.namespace.MicroprofilePropertyNames;
import org.eclipsefoundation.core.service.CachingService;
import org.jboss.resteasy.spi.NotImplementedYetException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.cache.CacheInvalidate;
import io.quarkus.cache.CacheInvalidateAll;
import io.quarkus.cache.CacheKey;
import io.quarkus.cache.CacheResult;

/**
 * Utililzes Quarkus caching extensions in order to cache and retrieve data.
 * 
 * @author Martin Lowe
 *
 * @param <T>
 */
@ApplicationScoped
public class QuarkusCachingService implements CachingService {
	private static final Logger LOGGER = LoggerFactory.getLogger(QuarkusCachingService.class);

	@ConfigProperty(name = MicroprofilePropertyNames.CACHE_SIZE_MAX, defaultValue = "10000")
	long maxSize;
	@ConfigProperty(name = MicroprofilePropertyNames.CACHE_TTL_MAX_SECONDS, defaultValue = "900")
	long ttlWrite;

	@Inject
	ParameterHelper paramHelper;
	
	@Override
	public <T> Optional<T> get(String id, MultivaluedMap<String, String> params, Class<?> rawType,
			Callable<? extends T> callable) {
		Objects.requireNonNull(callable);

		String cacheKey = getCacheKey(Objects.requireNonNull(id), Objects.requireNonNull(params),
				Objects.requireNonNull(rawType));
		LOGGER.debug("Retrieving cache value for '{}'", cacheKey);
		return Optional.ofNullable(get(cacheKey, callable));
	}

	@Override
	public Optional<Long> getExpiration(String id, MultivaluedMap<String, String> params, Class<?> type) {
		String cacheKey = getCacheKey(Objects.requireNonNull(id), Objects.requireNonNull(params),
				Objects.requireNonNull(type));
		try {
			return Optional.ofNullable(getExpiration(true, cacheKey));
		} catch (Exception e) {
			throw new RuntimeException("Error while retrieving expiration for cachekey: " + cacheKey);
		}
	}

	@Override
	public Set<String> getCacheKeys() {
		throw new NotImplementedYetException("This block has yet to be implemented");
	}

	@Override
	@CacheInvalidate(cacheName = "default")
	@CacheInvalidate(cacheName = "ttl")
	public void remove(@CacheKey String key) {
		LOGGER.debug("Removed cache key '{}' from TTL and default cache regions", key);
	}

	@Override
	@CacheInvalidateAll(cacheName = "default")
	@CacheInvalidateAll(cacheName = "ttl")
	public void removeAll() {
		LOGGER.debug("Cleared TTL and default cache regions");
	}

	@Override
	public long getMaxAge() {
		return TimeUnit.MILLISECONDS.convert(ttlWrite, TimeUnit.SECONDS);
	}

	/**
	 * Soft check of expiration, does not create new entries if no entry is present.
	 * 
	 * @param cacheKey key of cache item to retrieve TTL of.
	 * @return the epoch time that the given cache key expires, or null if the key
	 *         is not set.
	 */
	@SuppressWarnings("java:S3516") // suppressed as the cached entry can be returned when present
	public Long checkExpiration(String cacheKey) {
		try {
			// check for existing value, throwing out if none is found.
			return getExpiration(true, cacheKey);
		} catch (Exception e) {
			// no result found
			return null;
		}
	}

	@CacheResult(cacheName = "default")
	public <T> T get(@CacheKey String cacheKey, Callable<? extends T> callable) {
		T out = null;
		try {
			out = callable.call();
			// set internal expiration cache
			getExpiration(false, cacheKey);
		} catch (Exception e) {
			LOGGER.error("Error while creating cache entry for key '{}': \n", cacheKey, e);
		}
		return out;
	}

	@CacheResult(cacheName = "ttl")
	public long getExpiration(boolean throwIfMissing, @CacheKey String cacheKey) throws Exception {
		if (throwIfMissing) {
			throw new Exception("No TTL present for cache key '{}', not generating");
		}
		LOGGER.debug("Timeout for {}: {}", cacheKey, System.currentTimeMillis() + getMaxAge());
		return System.currentTimeMillis() + getMaxAge();
	}

	@Override
	public ParameterHelper getParameterHelper() {
		return paramHelper;
	}

}
