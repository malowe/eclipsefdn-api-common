/* Copyright (c) 2019 Eclipse Foundation and others.
 * This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License 2.0
 * which is available at http://www.eclipse.org/legal/epl-v20.html,
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.core.service;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;

import javax.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.helper.ParameterHelper;

import io.quarkus.arc.Unremovable;

/**
 * Interface defining the caching service to be used within the application.
 * 
 * @author Martin Lowe
 * @param <T> the type of object to be stored in the cache.
 */
@Unremovable
public interface CachingService {

	/**
	 * Returns an Optional object of type T, returning a cached object if available,
	 * otherwise using the callable to generate a value to be stored in the cache
	 * and returned.
	 * 
	 * @param id       the ID of the object to be stored in cache
	 * @param params   the query parameters for the current request
	 * @param callable a runnable that returns an object of type T
	 * @param rawType  the rawtype of data returned, which may be different than the
	 *                 actual data type returned (such as a returned list containing
	 *                 the raw type).
	 * @return the cached result
	 */
	<T> Optional<T> get(String id, MultivaluedMap<String, String> params, Class<?> rawType,
			Callable<? extends T> callable);

	/**
	 * Returns the expiration date in millis since epoch.
	 * 
	 * @param id     the ID of the object to be stored in cache
	 * @param params the query parameters for the current request
	 * @return an Optional expiration date for the current object if its set. If
	 *         there is no underlying data, then empty would be returned
	 */
	Optional<Long> getExpiration(String id, MultivaluedMap<String, String> params, Class<?> type);

	/**
	 * @return the max age of cache entries
	 */
	long getMaxAge();

	/**
	 * Retrieves a set of cache keys available to the current cache.
	 * 
	 * @return unmodifiable set of cache entry keys.
	 */
	Set<String> getCacheKeys();

	/**
	 * Removes cache entry for given cache entry key.
	 * 
	 * @param key cache entry key
	 */
	void remove(String key);

	/**
	 * Removes all cache entries.
	 */
	void removeAll();

	/**
	 * Promise presence of ParameterHelper to facilitate the generation of the cache
	 * key.
	 * 
	 * @return instance of the ParameterHelper class.
	 */
	ParameterHelper getParameterHelper();

	/**
	 * Generates a unique key based on the id of the item/set of items to be stored,
	 * as well as any passed parameters. This includes the passed map of parameters
	 * that may override what is present in the request.
	 * 
	 * @param id      identity string of the item to cache
	 * @param wrapper parameters associated with the request for information
	 * @param params  the map of override parameters for the current request
	 * @param type    type of object to be cached
	 * @return the unique cache key for the request.
	 */
	default String getCacheKey(String id, MultivaluedMap<String, String> params, Class<?> type) {
		StringBuilder sb = new StringBuilder();
		sb.append('[').append(type.getSimpleName()).append(']');
		sb.append("id:").append(id);

		// filter the parameters using a reference to the parameter helper
		ParameterHelper paramHelper = getParameterHelper();
		MultivaluedMap<String, String> filteredParams = paramHelper.filterUnknownParameters(params);
		// join all the non-empty params to the key to create distinct entries for
		// filtered values
		filteredParams.entrySet().stream().filter(e -> !e.getValue().isEmpty())
				.map(e -> e.getKey() + '=' + String.join(",", e.getValue())).forEach(s -> sb.append('|').append(s));
		return sb.toString();
	}
}
