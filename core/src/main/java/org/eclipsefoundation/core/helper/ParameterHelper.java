package org.eclipsefoundation.core.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.namespace.UrlParameterNamespace;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;

import io.quarkus.runtime.Startup;

/**
 * Helper for parameter lookups and filtering.
 * 
 * @author Martin Lowe
 *
 */
@Startup
@ApplicationScoped
public class ParameterHelper {

	@Inject
	Instance<UrlParameterNamespace> urlParamImpls;

	// maintain internal list of key names to reduce churn. These are only created
	// at build time
	private List<String> urlParamNames;

	/**
	 * Create the list of parameter key names once upon creation of this class
	 */
	@PostConstruct
	void initialize() {
		// use array list for better random access + lookups
		this.urlParamNames = new ArrayList<>();
		// populate a list with each
		urlParamImpls.forEach(
				namespace -> namespace.getParameters().stream().forEach(param -> urlParamNames.add(param.getName())));
	}

	/**
	 * Filters out unknown parameters by name/key. Creates and returns a new map
	 * with the known/tracked parameters. Tracked parameters are added through
	 * implementations of the {@link UrlParameterNamespace} interface.
	 * 
	 * @param src multivalued parameter map to filter
	 * @return a map containing the filtered parameter values
	 */
	public MultivaluedMap<String, String> filterUnknownParameters(MultivaluedMap<String, String> src) {
		MultivaluedMap<String, String> out = new MultivaluedMapImpl<>();
		Set<String> keys = src.keySet();
		for (String key : keys) {
			if (urlParamNames.contains(key)) {
				out.addAll(key, src.get(key));
			}
		}
		return out;
	}

	/**
	 * Returns a list of parameter names, as generated from reading in the
	 * parameters present in the {@link UrlParameterNamespace} implementations.
	 * 
	 * @return list of tracked parameter names.
	 */
	public List<String> getValidParameters() {
		return new ArrayList<>(urlParamNames);
	}
}
