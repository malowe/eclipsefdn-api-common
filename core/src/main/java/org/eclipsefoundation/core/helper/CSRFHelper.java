package org.eclipsefoundation.core.helper;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.exception.FinalUnauthorizedException;
import org.eclipsefoundation.core.model.AdditionalUserData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.undertow.util.HexConverter;

/**
 * Helper class for interacting with CSRF tokens within the server. Generates secure CSRF tokens and compares them to
 * the copy that exists within the current session object.
 * 
 * @author Martin Lowe
 *
 */
@Singleton
public final class CSRFHelper {
    public static final Logger LOGGER = LoggerFactory.getLogger(CSRFHelper.class);
    public static final String CSRF_HEADER_NAME = "x-csrf-token";

    @ConfigProperty(name = "security.token.salt", defaultValue = "short-salt")
    String salt;

    @ConfigProperty(name = "security.csrf.enabled", defaultValue = "false")
    boolean csrfEnabled;

    // cryptographically secure random number generator
    private SecureRandom rnd;

    @PostConstruct
    void init() {
        // create a secure Random impl using salt + timestamp bytes
        rnd = new SecureRandom(Long.toString(System.currentTimeMillis()).getBytes());
    }

    /**
     * Generate a new CSRF token that has been hardened to make it more difficult to predict.
     *
     * @return a cryptographically-secure CSRF token to use in a session.
     */
    public String getNewCSRFToken() {
        // use a random value salted with a configured static value
        byte[] bytes = rnd.generateSeed(24);
        String secureRnd = new String(bytes);
        // create a secure random secret to embed in the user session
        String preHash = secureRnd + salt;

        // create new digest to hash the result
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("Could not find SHA-256 algorithm to encode CSRF token", e);
        }
        // hash the results using the message digest
        byte[] array = md.digest(preHash.getBytes());
        // convert back to a hex string to act as a token
        return HexConverter.convertToHexString(array);
    }

    /**
     * Compares the passed CSRF token to the token for the current user session.
     *
     * @param aud session data for current user
     * @param passedCSRF the passed CSRF header data
     * @throws FinalUnauthorizedException when CSRF token is missing in the user data, the passed header value, or does
     * not match
     */
    public void compareCSRF(AdditionalUserData aud, String passedCSRF) {
        if (csrfEnabled) {
            LOGGER.debug("Comparing following tokens:\n{}\n{}", aud == null ? null : aud.getCsrf(), passedCSRF);
            if (aud == null || aud.getCsrf() == null) {
                throw new FinalUnauthorizedException(
                        "CSRF token not generated for current request and is required, refusing request");
            } else if (passedCSRF == null) {
                throw new FinalUnauthorizedException("No CSRF token passed for current request, refusing request");
            } else if (!passedCSRF.equals(aud.getCsrf())) {
                throw new FinalUnauthorizedException("CSRF tokens did not match, refusing request");
            }
        }
    }
}
