package org.eclipsefoundation.core.exception;

/**
 * Represents an unauthorized request with no redirect (standard UnauthorizedException gets routed
 * to OIDC login page when active, which is not desired).
 *
 * @author Martin Lowe
 */
public class FinalUnauthorizedException extends RuntimeException {

  public FinalUnauthorizedException(String message) {
    super(message);
  }

  /** */
  private static final long serialVersionUID = 1L;
}
