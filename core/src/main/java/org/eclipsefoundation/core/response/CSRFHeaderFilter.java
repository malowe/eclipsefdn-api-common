package org.eclipsefoundation.core.response;

import java.io.IOException;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.helper.CSRFHelper;
import org.eclipsefoundation.core.model.AdditionalUserData;

/**
 * Injects the CSRF header token into the response when enabled for a server.
 *
 * @author Martin Lowe
 */
@Provider
public class CSRFHeaderFilter implements ContainerResponseFilter {
    @ConfigProperty(name = "security.csrf.enabled", defaultValue = "false")
    boolean csrfEnabled;

    @Inject
    CSRFHelper csrf;
    @Inject
    AdditionalUserData aud;

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
            throws IOException {
        // only attach if CSRF is enabled for the current runtime
        if (csrfEnabled) {
            // generate a new token if none is yet present
            if (aud.getCsrf() == null) {
                aud.setCsrf(csrf.getNewCSRFToken());
            }
            // attach the current CSRF token as a header on the request
            responseContext.getHeaders().add(CSRFHelper.CSRF_HEADER_NAME, aud.getCsrf());
        }
    }
}
