package org.eclipsefoundation.core.namespace;

/**
 * Contains Microprofile property names used by this application.
 * 
 * @author Martin Lowe
 *
 */
public class MicroprofilePropertyNames {
	public static final String CACHE_TTL_MAX_SECONDS = "cache.ttl.write.seconds";
	public static final String CACHE_SIZE_MAX = "cache.max.size";
	
	private MicroprofilePropertyNames() {
	}
}
