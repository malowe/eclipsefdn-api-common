/*
 * Copyright (C) 2019 Eclipse Foundation and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
*/
package org.eclipsefoundation.core.namespace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.inject.Singleton;

/**
 * Namespace containing common URL parameters used throughout the API.
 * 
 * @author Martin Lowe
 */
@Singleton
public final class DefaultUrlParameterNames implements UrlParameterNamespace {
    public static final UrlParameter QUERY_STRING = new UrlParameter("q");
	public static final UrlParameter PAGE = new UrlParameter("page");
	public static final UrlParameter LIMIT = new UrlParameter("limit");
	public static final UrlParameter IDS = new UrlParameter("ids");
	public static final UrlParameter ID = new UrlParameter("id");
	
	private static final List<UrlParameter> params = Collections.unmodifiableList(Arrays.asList(QUERY_STRING,PAGE,LIMIT,IDS,ID));

	@Override
	public List<UrlParameter> getParameters() {
		return new ArrayList<>(params);
	}

	
}
