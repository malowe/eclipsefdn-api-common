package org.eclipsefoundation.search.namespace;

public enum IndexerResponseStatus {
	SUCCESSFUL, FAILED, MAINTENANCE;
}
