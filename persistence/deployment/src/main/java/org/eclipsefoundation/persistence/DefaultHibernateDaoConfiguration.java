package org.eclipsefoundation.persistence;

import java.util.List;
import java.util.Set;

import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.dao.impl.PlaceholderPersistenceDao;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.NodeBase;

import io.quarkus.arc.deployment.AdditionalBeanBuildItem;
import io.quarkus.deployment.annotations.BuildProducer;
import io.quarkus.deployment.annotations.BuildStep;
import io.quarkus.hibernate.orm.deployment.IgnorableNonIndexedClasses;
import io.quarkus.hibernate.orm.deployment.JpaEntitiesBuildItem;
import io.quarkus.hibernate.orm.deployment.NonJpaModelBuildItem;

public class DefaultHibernateDaoConfiguration {

	/**
	 * Register the required CDI beans into the application to be created at
	 * runtime. Currently this is only the Hibernate DAO, but this may be extended
	 * in the future.
	 * 
	 * @param beanProducer  binding point to add additional CDI beans
	 * @param combinedIndex Jandex index of the current compile-time class hierarchy
	 * @param capabilities  object containing the registered capabilities for the
	 *                      current package.
	 */
	@BuildStep
	void registerBeans(BuildProducer<AdditionalBeanBuildItem> beanProducer, JpaEntitiesBuildItem domainObjects,
			List<NonJpaModelBuildItem> nonJpaModelBuildItems) {
		// check if hibernate has been registered as an extension
		if (hasEntities(domainObjects, nonJpaModelBuildItems)) {
			// register the Hibernate DAO
			beanProducer.produce(
					AdditionalBeanBuildItem.builder().setUnremovable().addBeanClass(DefaultHibernateDao.class).build());
		} else {
			// register placeholder to unblock downstream dependencies. This can be removed
			// if a search extension is implemented
			beanProducer.produce(AdditionalBeanBuildItem.builder().setUnremovable()
					.addBeanClass(PlaceholderPersistenceDao.class).build());
		}
	}

	/**
	 * Marks the 2 node entity base classes as ignorable in the JPA entity scan by
	 * Hibernate ORM. Otherwise these entities get detected as JPA entities
	 * improperly and cause build issues.
	 * 
	 * @return class index containing the abstract base entity classes.
	 */
	@BuildStep
	IgnorableNonIndexedClasses registerNodeTypes() {
		return new IgnorableNonIndexedClasses(Set.of(BareNode.class.getName(), NodeBase.class.getName()));
	}

	private boolean hasEntities(JpaEntitiesBuildItem jpaEntities, List<NonJpaModelBuildItem> nonJpaModels) {
		return !jpaEntities.getEntityClassNames().isEmpty() || !nonJpaModels.isEmpty();
	}
}
