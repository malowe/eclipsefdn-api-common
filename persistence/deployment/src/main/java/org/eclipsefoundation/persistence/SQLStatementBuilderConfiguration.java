package org.eclipsefoundation.persistence;

import org.eclipsefoundation.persistence.config.ParameterizedSQLStatementBuilderConfiguration;

import io.quarkus.arc.deployment.AdditionalBeanBuildItem;
import io.quarkus.deployment.annotations.BuildProducer;
import io.quarkus.deployment.annotations.BuildStep;

public class SQLStatementBuilderConfiguration {

	/**
	 * Register the required CDI beans into the application to be created at
	 * runtime. Currently this is only the Hibernate DAO, but this may be extended
	 * in the future.
	 * 
	 * @param beanProducer binding point to add additional CDI beans
	 */
	@BuildStep
	void registerBeans(BuildProducer<AdditionalBeanBuildItem> beanProducer) {
		// register the configuration builder as unremovable. As this framework is meant
		// to be implemented externally and not used internally, this harness would be
		// removed in most cases
		beanProducer.produce(AdditionalBeanBuildItem.builder().setUnremovable()
				.addBeanClass(ParameterizedSQLStatementBuilderConfiguration.class).build());

	}
}
