package org.eclipsefoundation.persistence;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * Forces the application to start and compile a dunning test instance to
 * validate the server can start up with bare configurations (which it should).
 * 
 * @author Martin Lowe
 *
 */
@QuarkusTest
class BaseTest {

	@Test
	void running() {
	}
}
