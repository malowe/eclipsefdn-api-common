package org.eclipsefoundation.persistence.service.impl;

import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.service.FilterService;

@ApplicationScoped
public class DefaultFilterService implements FilterService {
  @Inject Instance<DtoFilter<?>> filters;

  @SuppressWarnings("unchecked")
  @Override
  public <T extends BareNode> DtoFilter<T> get(Class<T> target) {
    Optional<DtoFilter<?>> filter =
        filters.stream().filter(f -> f.getType().equals(target)).findFirst();
    if (filter.isPresent()) {
      return (DtoFilter<T>) filter.get();
    }
    return null;
  }
}
