package org.eclipsefoundation.persistence.config;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;

import org.eclipsefoundation.persistence.model.HQLGenerator;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;
import org.eclipsefoundation.persistence.model.SQLGenerator;

import io.quarkus.arc.DefaultBean;

@Dependent
public class ParameterizedSQLStatementBuilderConfiguration {
	
	@Produces
	@DefaultBean
	public ParameterizedSQLStatementBuilder parameterizedSQLStatementBuilder(SQLGenerator generator) {
		return new ParameterizedSQLStatementBuilder(generator);
	}
	
	@Produces
	@DefaultBean
	public SQLGenerator sqlGenerator() {
		return new HQLGenerator();
	}
}
