/* Copyright (c) 2019 Eclipse Foundation and others.
 * This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License 2.0
 * which is available at http://www.eclipse.org/legal/epl-v20.html,
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.persistence.dto;

import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Contains the basic fields for a node within Mongo
 * 
 * @author Martin Lowe
 */
@MappedSuperclass
public abstract class NodeBase extends BareNode {
	@Id
	@Column(unique = true, nullable = false, columnDefinition = "BINARY(16)")
	private UUID id;
	private String title;
	private String url;
	
	@Override
	public UUID getId() {
		return id;
	}
	
	public void setId(UUID id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Call to check whether the current node is valid.
	 * 
	 * @return whether the current node is valid.
	 */
	public boolean validate() {
		return url != null && !url.isBlank();
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), id, title, url);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		NodeBase other = (NodeBase) obj;
		return super.equals(obj) && Objects.equals(url, other.url) && Objects.equals(title, other.title);
	}
}
