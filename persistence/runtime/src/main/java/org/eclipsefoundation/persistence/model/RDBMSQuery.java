/* Copyright (c) 2019 Eclipse Foundation and others.
 * This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License 2.0
 * which is available at http://www.eclipse.org/legal/epl-v20.html,
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.persistence.model;

import java.util.List;
import java.util.Optional;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.helper.SortableHelper;
import org.eclipsefoundation.persistence.helper.SortableHelper.Sortable;
import org.eclipsefoundation.persistence.namespace.PersistenceUrlParameterNames;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Wrapper for initializing DB filters, sort clauses, and document type when
 * interacting with RDBMS'.
 * 
 * @author Martin Lowe
 */
public class RDBMSQuery<T extends BareNode> {
	private static final Logger LOGGER = LoggerFactory.getLogger(RDBMSQuery.class);

	private RequestWrapper wrapper;
	private MultivaluedMap<String, String> params;
	private DtoFilter<T> dtoFilter;

	private ParameterizedSQLStatement filter;
	private SortOrder order;

	public RDBMSQuery(RequestWrapper wrapper, DtoFilter<T> dtoFilter) {
		this(wrapper, dtoFilter, null);
	}

	public RDBMSQuery(RequestWrapper wrapper, DtoFilter<T> dtoFilter, MultivaluedMap<String, String> params) {
		this.wrapper = wrapper;
		this.dtoFilter = dtoFilter;
		this.params = new MultivaluedMapImpl<>();
		wrapper.asMap().forEach((key, valueList) -> this.params.addAll(key, valueList));
		if (params != null) {
			// replace the values set in the param map
			params.forEach((k, v) -> this.params.put(k, v));
		}
		init();
	}

	/**
	 * Initializes the query object using the current query string parameters and
	 * type object. This can be called again to reset the parameters if needed due
	 * to updated fields.
	 */
	public void init() {
		// clear old values if set to default
		this.filter = null;
		this.order = SortOrder.NONE;

		// get the filters for the current DTO
		this.filter = dtoFilter.getFilters(params, true);

		// get fields that make up the required fields to enable pagination and check
		String sortVal = params.getFirst(PersistenceUrlParameterNames.SORT.getName());
		if (sortVal != null) {
			SortOrder ord = SortOrder.getOrderFromValue(sortVal);
			// split sort string of `<fieldName> <SortOrder>`
			int idx = sortVal.indexOf(' ');
			// check if the sort string matches the RANDOM sort order
			if (SortOrder.RANDOM.equals(ord)) {
				filter.setOrder(SortOrder.RANDOM);
				this.order = SortOrder.RANDOM;
			} else if (ord != SortOrder.NONE) {
				setSort(sortVal.substring(0, idx), sortVal.substring(idx + 1));
			}
		}
	}

	/**
	 * Checks the URL parameter of {@link PersistenceUrlParameterNames.LIMIT} for a
	 * numeric value and returns it if present.
	 * 
	 * @return the value of the URL parameter
	 *         {@link PersistenceUrlParameterNames.LIMIT} if present and numeric,
	 *         otherwise returns -1.
	 */
	public int getLimit() {
		String limitVal = params.getFirst(DefaultUrlParameterNames.LIMIT.getName());
		if (limitVal != null && StringUtils.isNumeric(limitVal)) {
			return Integer.parseInt(limitVal);
		}
		return -1;
	}

	/**
	 * Used in cases where we know the exact offset to use (seeking in GraphQL
	 * queries is an example of this).
	 * 
	 * @return the manual offset when set and numeric, otherwise -1
	 */
	public int getManualOffset() {
		String limitVal = params.getFirst(PersistenceUrlParameterNames.MANUAL_OFFSET.getName());
		if (limitVal != null && StringUtils.isNumeric(limitVal)) {
			return Integer.parseInt(limitVal);
		}
		return -1;
	}

	private void setSort(String sortField, String sortOrder) {
		List<Sortable<?>> fields = SortableHelper.getSortableFields(getDocType());
		Optional<Sortable<?>> fieldContainer = SortableHelper.getSortableFieldByName(fields, sortField);
		if (fieldContainer.isPresent()) {
			this.order = SortOrder.getOrderByName(sortOrder);
			// add sorting query if the sortOrder matches a defined order
			switch (order) {
			case ASCENDING:
			case DESCENDING:
				this.filter.setOrder(order);
				this.filter.setSortField(sortField);
				break;
			case RANDOM:
				this.filter.setOrder(order);
				break;
			default:
				// intentionally empty, no sort
				break;
			}
		} else if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Field with name '{}' is not marked as sortable, skipping", sortField);
		}
	}

	/**
	 * @return the page of results to retrieve for query
	 */
	public int getPage() {
		String pageOpt = params.getFirst(DefaultUrlParameterNames.PAGE.getName());
		if (pageOpt != null && StringUtils.isNumeric(pageOpt)) {
			int tmpPage = Integer.parseInt(pageOpt);
			if (tmpPage > 0) {
				return tmpPage;
			}
		}
		return 1;
	}

	/**
	 * @return the filter
	 */
	public ParameterizedSQLStatement getFilter() {
		return this.filter;
	}

	/**
	 * @return the DTO filter
	 */
	public DtoFilter<T> getDTOFilter() {
		return this.dtoFilter;
	}

	/**
	 * @return the docType
	 */
	public Class<T> getDocType() {
		return dtoFilter.getType();
	}

	/**
	 * @return the wrapper
	 */
	public RequestWrapper getWrapper() {
		return wrapper;
	}

	/**
	 * @param wrapper the wrapper to set
	 */
	public void setWrapper(RequestWrapper wrapper) {
		this.wrapper = wrapper;
	}
}
