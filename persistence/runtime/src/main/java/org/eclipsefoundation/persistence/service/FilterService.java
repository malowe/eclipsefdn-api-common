package org.eclipsefoundation.persistence.service;

import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;

/**
 * Provides filters for the passed class reference to reduce the number of injections that need to
 * be handled by Graph endpoints.
 *
 * @author Martin Lowe
 */
public interface FilterService {

  /**
   * Returns a DTO filter if available for the given class.
   *
   * @param <T> the type of entity being filtered
   * @param target class ref for the target entity type
   * @return a filter instance for the given class if it exists, null otherwise
   */
  <T extends BareNode> DtoFilter<T> get(Class<T> target);
}
