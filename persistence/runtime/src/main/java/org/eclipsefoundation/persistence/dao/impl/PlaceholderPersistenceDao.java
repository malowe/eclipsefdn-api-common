package org.eclipsefoundation.persistence.dao.impl;

import java.util.List;

import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.model.RDBMSQuery;

/**
 * Produces a bare, non-functioning DAO for downstream build dependencies. This
 * enables downstream package builds when there are no available JPA entities.
 * 
 * @author Martin Lowe
 *
 */
public class PlaceholderPersistenceDao implements PersistenceDao {

	@Override
	public HealthCheckResponse call() {
		throw new IllegalStateException("Placeholder DAO should not be used in running instances");
	}

	@Override
	public <T extends BareNode> List<T> get(RDBMSQuery<T> q) {
		throw new IllegalStateException("Placeholder DAO should not be used in running instances");
	}

	@Override
	public <T extends BareNode> List<T> add(RDBMSQuery<T> q, List<T> documents) {
		throw new IllegalStateException("Placeholder DAO should not be used in running instances");

	}

	@Override
	public <T extends BareNode> void delete(RDBMSQuery<T> q) {
		throw new IllegalStateException("Placeholder DAO should not be used in running instances");
	}

	@Override
	public Long count(RDBMSQuery<?> q) {
		throw new IllegalStateException("Placeholder DAO should not be used in running instances");
	}

    @Override
    public <T extends BareNode> T getReference(Object id, Class<T> type) {
        throw new IllegalStateException("Placeholder DAO should not be used in running instances");
    }

}
