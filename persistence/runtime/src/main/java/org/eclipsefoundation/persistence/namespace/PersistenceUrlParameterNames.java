/*
 * Copyright (C) 2019 Eclipse Foundation and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
*/
package org.eclipsefoundation.persistence.namespace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.inject.Singleton;

import org.eclipsefoundation.core.namespace.UrlParameterNamespace;

/**
 * Namespace containing persistence based URL parameters used throughout the
 * API.
 * 
 * @author Martin Lowe
 */
@Singleton
public final class PersistenceUrlParameterNames implements UrlParameterNamespace {
	public static final UrlParameter SORT = new UrlParameter("sort");
	public static final UrlParameter MANUAL_OFFSET = new UrlParameter("manual_offset");

	private static final List<UrlParameter> params = Collections.unmodifiableList(Arrays.asList(SORT, MANUAL_OFFSET));

	@Override
	public List<UrlParameter> getParameters() {
		return new ArrayList<>(params);
	}

}
