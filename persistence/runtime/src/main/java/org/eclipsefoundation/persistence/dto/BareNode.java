package org.eclipsefoundation.persistence.dto;

import java.util.Objects;

import javax.persistence.MappedSuperclass;

/**
 * Represents a bare node with just ID and title for sake of persistence.
 * 
 * @author Martin Lowe
 */
@MappedSuperclass
public abstract class BareNode {

	/**
	 * @return the id
	 */
	public abstract Object getId();

	/**
	 * Initializes lazy fields through access. This is used to avoid issues with JPA
	 * sessions closing before access.
	 */
	public void initializeLazyFields() {
		// intentionally empty
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getId());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BareNode other = (BareNode) obj;
		return super.equals(obj) && Objects.equals(getId(), other.getId());
	}
}
