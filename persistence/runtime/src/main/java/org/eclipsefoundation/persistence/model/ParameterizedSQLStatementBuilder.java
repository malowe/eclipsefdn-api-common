package org.eclipsefoundation.persistence.model;

public class ParameterizedSQLStatementBuilder {

	SQLGenerator generator;
	
	public ParameterizedSQLStatementBuilder(SQLGenerator generator) {
		this.generator = generator;
	}
	
	
	public ParameterizedSQLStatement build(DtoTable base) {
		return new ParameterizedSQLStatement(base, generator);
	}
	
}
